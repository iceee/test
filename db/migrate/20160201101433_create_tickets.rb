class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.string :phone
      t.string :subject
      t.text :note

      t.timestamps null: false
    end
  end
end
