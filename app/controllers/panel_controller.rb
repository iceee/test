class PanelController < ApplicationController

	before_action :set_article, only: [ :delete_article , :edit ,:update]
	before_action :set_admin , only: [:delete_user]
  before_action :set_contact , only: [:contact_final]

	require 'bcrypt'
	before_filter :time_auth
	before_filter :authorize


	def mail_change
	end

	def change_e
		user = User.find_by_id(session[:user_id])
		unless params[:email_new].empty?
			if params[:email_retype]== params[:email_new]
				User.where(session[:user_id]).limit(1).update_all("'email' = '#{params[:email_new]}'")

				flash[:notice]='ایمیل تغییر یافت'
				redirect_to '/panel'
			else
				flash[:notice2]='ایمیل ها مطابقت ندارند'
				redirect_to '/e-mail'
			end
		else
			flash[:notice2]='فیلد ایمیل خالی است'
			redirect_to'/e-mail'
		end
	end


	def change_p
		user = User.find_by_id(session[:user_id])
		hashed_password=BCrypt::Password.create(params[:password_new])
		if user.authenticate(params[:password_entered])
			unless params[:password_new].empty?
				if params[:password_retype]== params[:password_new]

					User.where(session[:user_id]).limit(1).update_all("'password_digest' = '#{hashed_password}'")
					flash[:notice]='کلمه عبور عوض شد'
					redirect_to '/panel'
				else
					flash[:notice2]='پسورد ها مطابقت ندارند'
					redirect_to '/password'
				end
			else
				flash[:notice2]='فیلد کلمه عبور خالی است'
				redirect_to'/password'
			end
		else
			flash[:notice2]= 'اشتباهی رخ داده است'
			redirect_to'/password'
		end
	end


	def new_article

	end


	def edit_article
		@article=Article.all.order('id DESC')
    end

    def edit
    end


    def update
     respond_to do |format|
      if @article_find.update(article_params)
        format.html { redirect_to :edit_article, notice: 'مطلب با موفقیت به روزرسانی شد' }
        format.json { render :edit_article, status: :ok, location: @article_find }
      else
        format.html { render :edit }
        format.json { render json: @article_find.errors, status: :unprocessable_entity }
      end
    end
    end

 	def delete_article

 		 @article_find.destroy

 		 respond_to do |format|
       format.html { redirect_to '/edit_article', notice: 'مطلب حذف شد' }
       format.json { head :no_content }
     end

 	end

	def create_article
		


  

		article = Article.new(article_params) 
		if article.save
			late = Article.maximum('id')
  tmp = params[:article][:pic].tempfile
destiny_file = File.join('public', 'upload', "art#{late}")
FileUtils.move tmp.path, destiny_file

			flash[:notice] = 'مطلب پست شد'
			redirect_to'/panel'
		else
			flash[:notice2] = 'اشتباهی رخ داده است'
			redirect_to '/panel'
		end


	
end
	
	def delete_admin
		if current_user.username == "admin"
			@user=User.all

		else
			flash[:notice2]='فقط مدیر ارشد دسترسی دارد'
			redirect_to '/panel'
		end

	end

	def delete_user
	if current_user.username == "admin"
 		 @admin_find.destroy

 		 respond_to do |format|
       format.html { redirect_to '/delete_admin', notice: 'مدیر حذف شد' }
       format.json { head :no_content }
   		end
    else
    	flash[:notice2]='فقط مدیر ارشد دسترسی دارد'
			redirect_to '/panel'
		end



	end

	def view_ticket
		@ticket = Ticket.all
	end

	def del_ticket
		flash[:notice3]='آیا مطمینید میخواهید همه رکورد ها را پاک کنید؟ '
		redirect_to '/ticket_view'
	end
	def del_ticket_final
		Ticket.destroy_all
		flash[:notice]='رکورد ها پاک شدند.'
		redirect_to '/ticket_view'
	end

	def contact_final

respond_to do |format|
 if @contact_find.update(infos_c)
	 format.html { redirect_to :edit_contact, notice: 'بروزرسانی انجام شد' }
	 format.json { render :edit_contact, status: :ok, location: @contact_find }
 else
	 format.html { render :edit_contact }
	 format.json { render json: @contact_find.errors, status: :unprocessable_entity }
 end
end


end



    def set_article
      @article_find = Article.find(params[:id])
    end

		def set_contact
			@contact_find = Info.find(1)
		end

    def set_admin
      @admin_find = User.find(params[:id])
    end

    def article_params

		params.require(:article).permit(:title,:content) 
	
	end

def infos_c
params.require(:info).permit(:text1,:text2)

end



end
