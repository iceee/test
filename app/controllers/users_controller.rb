class UsersController < ApplicationController

before_filter :time_auth
  before_filter :authorize

   def new
  end

  def create
    user = User.new(user_params)
    if user.save

      redirect_to '/panel'
    else
        flash[:notice2] = 'اشتباهی رخ داده است'
      redirect_to '/addadmin'
    end
  end

private

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
