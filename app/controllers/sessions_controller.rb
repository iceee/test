class SessionsController < ApplicationController
   def create
    if params[:z_rand] == params[:answer]
    user = User.find_by_username(params[:username])

    if user && user.authenticate(params[:password])
 	  session[:time_exp]=Time.now + 30.minutes
      session[:user_id] = user.id
      redirect_to '/panel'
    else
    	flash[:notice2] = 'اشتباهی رخ داده است'
      redirect_to '/login'
    end
    else
     flash[:notice2] = 'پاسخ امنیتی نادرست است'
      redirect_to '/login' 
  end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice2] = 'خارج شدید'
    redirect_to '/login'
  end
end
