class ApplicationController < ActionController::Base
   protect_from_forgery with: :exception

  def current_user
    
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    redirect_to '/login' unless current_user
  end

  def time_auth
  	if session[:time_exp] && (session[:time_exp] <	 Time.now )
  		redirect_to '/logout'
  	end

  end


end
